import { Component } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  public url = 'http://127.0.0.1:5000';
  public numbers: any;
  public text: any;

  constructor(private http: HttpClient) {

  }

  public testServer() {
    this.http.get(this.url).subscribe((res) => {
      (this as any).text = (res as any).text;
    });
  }

  public findMax() {
    this.http.post(this.url + '/max', JSON.stringify({numbers: this.numbers}), httpOptions);
  }
}

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};
