import { Inject, Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { WINDOW } from '../window-providers';
import {map} from "rxjs/internal/operators";

@Injectable()
export class HttpConnectionService {

  constructor(@Inject(WINDOW) private window: Window,
              private http: HttpClient,) {}

  get prefix(){
    const domain = this.window.location.hostname;
    const port = this.window.location.port;
    return `http://${domain}${port === '4200' ? `:5000` : ''}`;
  }

  get httpOptions(){
    return {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
  }

  getTestData() {
    return this.http.get(`${this.prefix}`)
      .pipe(map(response => {
        return response as any[];
      }));
  }
}
