import { Component, OnInit } from '@angular/core';
import { HttpConnectionService } from "../shared/http-connection.service";

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {
  data: any;

  constructor(private httpService: HttpConnectionService) {
    this.getDataFromServer();
  }

  ngOnInit() {
  }

  getDataFromServer() {
    this.httpService.getTestData().subscribe((res)=>{
      this.data = res.text;
    });
  }

}
